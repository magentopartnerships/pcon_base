<?php

namespace Pcon\Base\Helper;

use Magento\Framework\App\Helper\AbstractHelper;
use Magento\Framework\App\Helper\Context;

class Data extends AbstractHelper
{
    //EAI WSDL
    const SESSION_CLIENT_PATH = '/EAI/Session?WSDL';
    const CATALOG_CLIENT_PATH = '/EAI/Catalog?WSDL';
    const BASKET_CLIENT_PATH = '/EAI/Basket?WSDL';

    const XML_PATH_GATEKEEPER_ID = 'base/general/gatekeeper_id';
    const XML_PATH_IMAGE_OPTION_FORMAT = 'base/image_options/format';
    const XML_PATH_IMAGE_OPTION_HIDE_SUB_ARTICLES = 'base/image_options/hide_sub_articles';
    const XML_PATH_IMAGE_OPTION_SHADOW_PLANE_FILTER = 'base/image_options/shadow_plane_filter';
    const XML_PATH_IMAGE_OPTION_SHADOW_PLANE_MAP_SIZE = 'base/image_options/shadow_plane_map_size';
    const XML_PATH_IMAGE_OPTION_SHADOW_PLANE_MAX_HEIGHT = 'base/image_options/shadow_plane_max_height';
    const XML_PATH_IMAGE_OPTION_SHADOW_PLANE_SMOOTHNESS = 'base/image_options/shadow_plane_smoothness';
    const XML_PATH_IMAGE_OPTION_SHADOW_COLOR = 'base/image_options/shadow_plane_color';

    const XML_PATH_ARTICLE_PACKAGE_ID = 'base/general/article_package_id';
    const XML_PATH_BASE_ARTICLE_NUMBER = 'base/general/base_article_number';

    const XML_PATH_CATALOG_ID = 'base/general/catalog_id';
    const XML_PATH_CATALOG_PACKAGE_ID = 'base/general/catalog_package_id';

    // obj - factory
    protected $_scopeConfig;
    protected $_soapClientFactory;

    public function __construct(
        \Magento\Framework\App\Config\ScopeConfigInterface $scopeConfig,
        \Magento\Framework\Webapi\Soap\ClientFactory $soapClientFactory,
        Context $context
    )
    {
        $this->_scopeConfig = $scopeConfig;
        $this->_soapClientFactory = $soapClientFactory;
        parent::__construct($context);
    }

    public function getConfig($xmlPath) {
        return $this->_scopeConfig->getValue($xmlPath, \Magento\Store\Model\ScopeInterface::SCOPE_STORE);
    }


    // render bild
    public function getGeneratedImage($client, $sessionId, $itemId, $options)
    {
        return $this->getImage($this->getArticleImageLink($client, $sessionId, $itemId, $options));
    }

    public function getImage($url)
    {
        return '<img src="' . $url . '"/>';
    }

    public function getArticleImageLink($client, $sessionId, $itemId, $options)
    {
        $res = $client->getGeneratedImage(array('sessionId' => $sessionId, 'itemId' => $itemId, 'options' => $options));
        return $res->return;
    }

    public function createClient($url)
    {
        $options = array(
            "exceptions" => 1,
            'trace' => 1,
            'encoding' => 'UTF-8'
        );

        try {
            $result = $this->_soapClientFactory->create($url, $options);
        } catch (SoapFault $E) {
            echo $E->faultstring;
        }
        return $result;
    }

    public static function getSession($gatekeeperId) {
        $content = file_get_contents("https://eaiws-server.pcon-solutions.com/v2/session/" . $gatekeeperId);
        $reply = json_decode($content);

        // Note: the php client needs to parse the WSDL and XSD file, but the
        // URL of the XSD file is incorrect. The `WSDLFIX` plugin can be used
        // as an workaround.
        if (strpos($reply->server, 'https') === 0) {
            $reply->server .= 'EAIWS/WSDL';
        }

        return $reply;
    }

    public function openSession($client, $startupFile)
    {
        $res = $client->openSession(array(
            "args" => array(
                "first" => "-startup",
                "second" => $startupFile
            )
        ));

        return $res->return;
    }

    public function insertOFMLArticle($client, $sessionId, $fatherId, $beforeId, $articlePackageId, $baseArticleNumber, $catalogId, $catalogPackageId, $variantCode = '')
    {
        $res = $client->insertOFMLArticle(
            array(
                'sessionId' => $sessionId,
                "fatherId" => '',
                "beforeId" => '',
                "insertInfo" => array(
                    "articlePackageId" => $articlePackageId,
                    "baseArticleNumber" => $baseArticleNumber,
                    "catalogId" => $catalogId,
                    "manufacturerId" => '',
                    "catalogPackageId" => $catalogPackageId,
                    "seriesId" => '',
                    "variantCode" => $variantCode
                ),
                "options" => ''
            ));

        return $res->return;
    }

    public function getImageOptions(){
        $options = array(
            'format=' . $this->getConfig(self::XML_PATH_IMAGE_OPTION_FORMAT),
            'hideSubArticles=' . $this->getConfig(self::XML_PATH_IMAGE_OPTION_HIDE_SUB_ARTICLES),
            'shadowPlane.filter=' . $this->getConfig(self::XML_PATH_IMAGE_OPTION_SHADOW_PLANE_FILTER),
            'shadowPlane.mapSize=' . $this->getConfig(self::XML_PATH_IMAGE_OPTION_SHADOW_PLANE_MAP_SIZE),
            'shadowPlane.maxHeight=' . $this->getConfig(self::XML_PATH_IMAGE_OPTION_SHADOW_PLANE_MAX_HEIGHT),
            'shadowPlane.smoothness=' . $this->getConfig(self::XML_PATH_IMAGE_OPTION_SHADOW_PLANE_SMOOTHNESS),
            'shadowPlane.color=' . $this->getConfig(self::XML_PATH_IMAGE_OPTION_SHADOW_COLOR)
        );
        return $options;
    }
	
    public function listCatalogItems($client, $sessionId, $path, $keys = array())
    {
        return $client->listCatalogItems(
            array(
                'sessionId' => $sessionId,
                "path" => $path,
                "options" => array(
                    "itemTypes" => "Article",
                    "displayMode" => "All",
                    "descriptors" => true,
                    "subCategories" => true,
                    "synonyms" => true
                )
            ));
    }

    public function getCatalogItem($client, $sessionId, $path)
    {
        return $client->getCatalogItem(
            array(
                'sessionId' => $sessionId,
                "displMode" => 'All',
                "insModes" => 1,
                "path" => $path
            ));
    }


    public function example(){
        // The gatekeeper service takes care of the session creation and returns a valid
        // session id and the URL of the EAIWS server to be used.
        $_gatekeeperId = $this->getConfig(self::XML_PATH_GATEKEEPER_ID);

        // information about server, port and startup (if the gatekeeper is not used)
        $_server = null;
        $_startup = null;

        $sessionId = null;

        // example image parameters
        // for more see specification
        $_imageOptions = $this->getImageOptions();

        if ($_gatekeeperId != null) {
            $reply = self::getSession($_gatekeeperId);
            $sessionId = $reply->sessionId;
            $_server = $reply->server;
        }

        // client definitions
        $sessionClient = $this->createClient($_server.self::SESSION_CLIENT_PATH);
        $catalogClient = $this->createClient($_server.self::CATALOG_CLIENT_PATH);
        $basketClient = $this->createClient($_server.self::BASKET_CLIENT_PATH);

        if ($_gatekeeperId == null) {
            $sessionId = $this->openSession($sessionClient, $_startup);
        }
		
		// catalog info
        //level 0:
        $_path = array();
        $res = $this->listCatalogItems($catalogClient, $sessionId, $_path);

        $_articlePackageId = $this->getConfig(self::XML_PATH_ARTICLE_PACKAGE_ID);
        $_baseArticleNumber = $this->getConfig(self::XML_PATH_BASE_ARTICLE_NUMBER);
        $_catalogId = $this->getConfig(self::XML_PATH_CATALOG_ID);
        $_catalogPackageId = $this->getConfig(self::XML_PATH_CATALOG_PACKAGE_ID);
        $_variantCode = '';
        // get article information
        if ($_articlePackageId and $_baseArticleNumber and $_catalogId and $_catalogPackageId) {
            $itemId = $this->insertOFMLArticle(
                $basketClient,
                $sessionId,
                '',
                '',
                $_articlePackageId,
                $_baseArticleNumber,
                $_catalogId,
                $_catalogPackageId,
                $_variantCode
            );
            echo "itemId: $itemId<br><br>";
        }

        // generate article image
        $res = $this->getGeneratedImage($basketClient, $sessionId, $itemId, $_imageOptions);
        echo "$res<br><br>";

        return $res;
    }
}