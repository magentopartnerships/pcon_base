<?php

namespace Pcon\Base\Setup;
use Magento\Eav\Setup\EavSetup;
use Magento\Eav\Setup\EavSetupFactory;
use Magento\Framework\Setup\InstallDataInterface;
use Magento\Framework\Setup\ModuleContextInterface;
use Magento\Framework\Setup\ModuleDataSetupInterface;
use Magento\Ui\Component\Form\Element\DataType\Text;

class InstallData implements InstallDataInterface
{
    private $eavSetupFactory;

    public function __construct(EavSetupFactory $eavSetupFactory)
    {
        $this->eavSetupFactory = $eavSetupFactory;
    }

    public function install(ModuleDataSetupInterface $setup, ModuleContextInterface $context)
    {
        $eavSetup = $this->eavSetupFactory->create(['setup' => $setup]);
        $eavSetup->addAttribute(
            \Magento\Catalog\Model\Product::ENTITY,
            'art_series',
            [
                'type' => 'text',
                'label' => 'Art Series',
                'required' => false,
                'searchable' => true,
                'filterable' => false,
                'is_use_define' => true,
                'class' => 'art-series',

                'group' => '',
                'input' => 'text',
                'source' => '',
                'global' => \Magento\Eav\Model\Entity\Attribute\ScopedAttributeInterface::SCOPE_STORE,
                'visible' => true,
                'user_defined' => false,
                'default' => '',
                'sort_order' => 8,
                'apply_to' => '',
                'note' => '',
            ]
        );
        $eavSetup->addAttribute(
            \Magento\Catalog\Model\Product::ENTITY,
            'art_base',
            [
                'type' => 'text',
                'label' => 'Art Base',
                'required' => false,
                'searchable' => true,
                'filterable' => false,
                'is_use_define' => true,
                'class' => 'art-base',

                'group' => '',
                'input' => 'text',
                'source' => '',
                'global' => \Magento\Eav\Model\Entity\Attribute\ScopedAttributeInterface::SCOPE_STORE,
                'visible' => true,
                'user_defined' => false,
                'default' => '',
                'sort_order' => 8,
                'apply_to' => '',
                'note' => '',
            ]
        );
        $eavSetup->addAttribute(
            \Magento\Catalog\Model\Product::ENTITY,
            'art_final',
            [
                'type' => 'text',
                'label' => 'Art Final',
                'required' => false,
                'searchable' => true,
                'filterable' => false,
                'is_use_define' => true,
                'class' => 'art-final',

                'group' => '',
                'input' => 'text',
                'source' => '',
                'global' => \Magento\Eav\Model\Entity\Attribute\ScopedAttributeInterface::SCOPE_STORE,
                'visible' => true,
                'user_defined' => false,
                'default' => '',
                'sort_order' => 8,
                'apply_to' => '',
                'note' => '',
            ]
        );

        $eavSetup->addAttribute(
            \Magento\Catalog\Model\Product::ENTITY,
            'art',
            [
                'type' => 'text',
                'label' => 'Art',
                'required' => false,
                'searchable' => true,
                'filterable' => false,
                'is_use_define' => true,
                'class' => 'art',

                'group' => '',
                'input' => 'text',
                'source' => '',
                'global' => \Magento\Eav\Model\Entity\Attribute\ScopedAttributeInterface::SCOPE_STORE,
                'visible' => true,
                'user_defined' => false,
                'default' => '',
                'sort_order' => 8,
                'apply_to' => '',
                'note' => '',
            ]
        );

        $eavSetup->addAttribute(
            \Magento\Catalog\Model\Product::ENTITY,
            'varcode',
            [
                'type' => 'text',
                'label' => 'Varcode',
                'required' => false,
                'searchable' => true,
                'filterable' => false,
                'is_use_define' => true,
                'class' => 'varcode',

                'group' => '',
                'input' => 'text',
                'source' => '',
                'global' => \Magento\Eav\Model\Entity\Attribute\ScopedAttributeInterface::SCOPE_STORE,
                'visible' => true,
                'user_defined' => false,
                'default' => '',
                'sort_order' => 8,
                'apply_to' => '',
                'note' => '',
            ]
        );

        $eavSetup->addAttribute(
            \Magento\Catalog\Model\Product::ENTITY,
            'art_ofml_varcode',
            [
                'type' => 'text',
                'label' => 'Art Ofml Varcode',
                'required' => false,
                'searchable' => true,
                'filterable' => false,
                'is_use_define' => true,
                'class' => 'art-ofml-varcode',

                'group' => '',
                'input' => 'text',
                'source' => '',
                'global' => \Magento\Eav\Model\Entity\Attribute\ScopedAttributeInterface::SCOPE_STORE,
                'visible' => true,
                'user_defined' => false,
                'default' => '',
                'sort_order' => 8,
                'apply_to' => '',
                'note' => '',
            ]
        );
    }
}